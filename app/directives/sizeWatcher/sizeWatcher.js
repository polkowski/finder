finderApp.directive("sizeWatcher", ['$timeout', function ($timeout) {
    return {
        scope: {
            sizeWatcherHeight: '=',
            sizeWatcherWidth: '=',
        },
        link: function( scope, elem, attrs ){
            function checkSize(){
                scope.sizeWatcherHeight = elem.prop('offsetHeight');
                scope.sizeWatcherWidth = elem.prop('clientWidth');
                $timeout( checkSize, 10 );
            }
            checkSize();
        }
    };
}])