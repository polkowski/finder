finderApp.directive('leftMenu', function () {
    return {
        controller: function ($scope, $http) {
            $http.get('json/left-menu.json').success(function (data) {
                $scope.menu = data;
                $scope.submenu = data[0].sub;
            });
            $scope.changeSub = function (menuId) {
                $scope.submenu = $scope.menu[menuId].sub;
            }
        },
        restrict: 'E',
        templateUrl: 'app/directives/leftMenuDrct/menu.tpl.htm'
    };
});