// app.js

// create the module and name it finderApp
// also include ngRoute for all our routing needs
var finderApp = angular.module('finderApp', ['ngRoute', 'ngAnimate']);

// configure our routes
finderApp.config(function ($routeProvider) {
    $routeProvider

        // route for the introduction page
        .when('/', {
            title: 'Introduction',
            templateUrl: 'views/pages/introduction.html',
            controller: 'IntroductionCtrl'
        })

        // route for the guidelines page
        .when('/guidelines', {
            title: 'Guidelines',
            templateUrl: 'views/pages/guidelines.html',
            controller: 'GuidelinesController'
        })

        // route for the components page
        .when('/components', {
            title: 'Components',
            templateUrl: 'views/pages/components.html',
            controller: 'ComponentsController'
        })

        // route for the constructs page
        .when('/constructs', {
            title: 'Constructs',
            templateUrl: 'views/pages/constructs.html',
            controller: 'ConstructsController'
        })

        // route for the layouts page
        .when('/layouts', {
            title: 'Layouts',
            templateUrl: 'views/pages/layouts.html',
            controller: 'LayoutsController'
        })

        // route for the github page
        .when('/github', {
            title: 'Github',
            templateUrl: 'views/pages/github.html',
            controller: 'GithubController'
        })

        .otherwise({ redirectTo: '/' });

});

finderApp.run(['$rootScope', function ($rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.title = current.$$route.title;
    });
    $rootScope.Object = Object;
}]);



//console.log('scope is', $scope)