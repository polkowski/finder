finderApp.controller('moreContent', function ($scope, $timeout, $element) {
    $scope.Object = Object;

    $scope.Appear = false;
    $scope.ShowHideAdditionalContent = function () {
        $scope.Appear = $scope.Appear ? false : true;
    };
    $scope.Saving = false;
    $scope.FakeSavingDuration = '8500';
    $scope.SaveProgress = function () {
        $scope.Saving = true;
        $timeout(function () {
            $scope.Saving = false;
        }, $scope.FakeSavingDuration);
    };

});